//
//  ContentView.swift
//  WeSplit
//
//  Created by Bishal Dahal on 3/6/21.
//

import SwiftUI

struct ContentView: View {
    let students = ["Harry", "Hermione", "Ron"]
        @State private var selectedStudent = 0

        var body: some View {
            VStack {
                Picker("Select your student name as test", selection: $selectedStudent) {
                    ForEach(0 ..< students.count) {
                        Text(self.students[$0])
                            .font(.title)
                    }
                }
                Text("You chose: Student # \(students[selectedStudent])")
            }
        }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
