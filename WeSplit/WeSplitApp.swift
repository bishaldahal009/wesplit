//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Bishal Dahal on 3/6/21.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
